package Controller;

import Environment.DirectionEnum;
import Environment.Door;
import Environment.Floor;
import Environment.Lift;
import Environment.LiftDoor;
import Environment.Motor;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Controller {

    BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));
    private Lift lift;
    private List<Floor> floors;
    public static final int liftSpeed = 4;
    public static final int doorSpeed = 2;
    public static final int floorDistance = 20;
    public static final int floorAmount = 3;

    public static int getDoorWidth() {
        return doorSpeed * doorSpeed;
    }

    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.init();
        controller.startLiftControllerLoop();
        controller.startFloorControllerLoop();



    }

    public void init() {
        initLift(floorAmount);
        initFloors(floorAmount);
    }

    private void initLift(int floorAmount) {
        LiftDoor liftDoor = new LiftDoor(Boolean.FALSE, Boolean.TRUE);
        Motor motor = new Motor(liftSpeed, DirectionEnum.NONE);
        lift = new Lift(new Boolean[floorAmount], Boolean.FALSE, 0, new LinkedList<Floor>(), liftDoor, motor);
    }

    private void initFloors(int floorAmount) {
        floors = new ArrayList<Floor>();
        Door door = new Door(0, new Motor(doorSpeed, DirectionEnum.NONE));
        for (int i = 0; i < floorAmount; i++) {
            floors.add(new Floor(i + 1, Boolean.FALSE, i * floorDistance, Boolean.FALSE, door));
        }
    }

    private void startLiftControllerLoop() {
        Thread liftController = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    liftMainLoop();
                }
            }
        });
        liftController.start();
    }

    private void startFloorControllerLoop() {
        Thread floorController = new Thread(new Runnable() {
            public void run() {
                floorUserInput();
            }
        });
        floorController.start();
    }

    private void floorUserInput() throws NumberFormatException {
        while (true) {
            try {
                System.out.println("Push button on floor (1-" + floorAmount + ")");
                int floor = Integer.parseInt(inputReader.readLine());
                floor = floor - 1;
                if (floor < 0 || floor >= floorAmount) {
                    System.out.println("Floor numbers has to be in the range 1-" + floorAmount);
                } else {
                    floors.get(floor).setButton(Boolean.TRUE);
                    lift.addFloorToQueue(floors.get(floor));

                }
            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void openDoors(Floor floor) {
        System.out.println("Opening doors at floor " + floor.getFloor());
        lift.getDoor().openFully();
        floor.getDoor().openFully();
        System.out.println("Doors open at floor " + floor.getFloor());
    }

    public static void waitSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void closeDoors(Floor floor) {
        System.out.println("Closing doors at floor " + floor.getFloor());
        while (lift.getDoor().getPosition() > 0) {
            if (lift.getDoor().getObstacle()) {
                System.out.println("Obstacle in doorway. Waiting 1 sec");
                waitSeconds(1);
            } else {
                System.out.println("Closing doors at floor " +"("+ lift.getDoor().getPosition()+"/0)");
                lift.getDoor().close();
                floor.getDoor().close();
            }
        }
        System.out.println("Doors closed at floor " + floor.getFloor());
    }

    private void liftMainLoop() {
        if (lift.isIdle() && !lift.getQueue().isEmpty() && lift.getQueue() != null) {
            Floor nextStop = pickUpFromFloor();
            if (!lift.getDoor().getClosed()) {
                closeDoors(nextStop);
                lift.setIdle(Boolean.TRUE);
                System.out.println("Lift Idle at floor " + nextStop.getFloor());
            } else {
                transportToFloor();
                if (!lift.getDoor().getClosed()) {
                    closeDoors(nextStop);
                    lift.setIdle(Boolean.TRUE);
                    System.out.println("Lift Idle at floor " + nextStop.getFloor());
                }
            }
        }
    }

    private int liftUserInput() throws NumberFormatException {
        try {
            int floor;
            do {
                System.out.println("Push button in lift (1-" + floorAmount + ")");
                floor = Integer.parseInt(inputReader.readLine());
                floor = floor - 1;
                if (floor < 0 || floor >= floorAmount) {
                    System.out.println("Floor numbers has to be in the range 1-" + floorAmount);
                } else {
                    return floor;
                }
            } while (floor < 0 || floor >= floorAmount);

        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    private Floor pickUpFromFloor() {
        Floor nextStop = lift.getQueue().remove(0);
        lift.setIdle(Boolean.FALSE);
        lift.moveToFloor(nextStop);
        System.out.println("At floor " + nextStop.getFloor());
        openDoors(nextStop);
        int counter = 0;
        Thread liftPassenger = new Thread(new Runnable() {
            public void run() {
                Controller.waitSeconds(5);
                lift.getDoor().setObstacle(Boolean.TRUE);
                Controller.waitSeconds(2);
                lift.getDoor().setObstacle(Boolean.FALSE);
            }
        });
        liftPassenger.start();
        while (counter < 20) {
            if (lift.getDoor().getObstacle()) {
                closeDoors(nextStop);
                continue;
            }
            counter++;
            waitSeconds(1);
        }
        return nextStop;
    }

    private void transportToFloor() throws NumberFormatException {
        Floor nextStop;
        lift.setIdle(Boolean.FALSE);
        nextStop = floors.get(liftUserInput());
        lift.moveToFloor(nextStop);
        System.out.println("Moving to floor " + nextStop.getFloor());
        openDoors(nextStop);
        int counter = 0;
        Thread liftPassenger = new Thread(new Runnable() {
            public void run() {
                Controller.waitSeconds(2);
                lift.getDoor().setObstacle(Boolean.TRUE);
                Controller.waitSeconds(2);
                lift.getDoor().setObstacle(Boolean.FALSE);
            }
        });
        liftPassenger.start();
        while (counter < 20) {
            if (lift.getDoor().getObstacle()) {
                waitSeconds(5);
                closeDoors(nextStop);
                lift.setIdle(Boolean.TRUE);
                continue;
            }
            counter++;
            waitSeconds(1);
        }
    }
}