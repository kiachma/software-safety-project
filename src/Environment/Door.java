package Environment;

import static Environment.DirectionEnum.OPEN;

public class Door {

    private Integer position;
    private Motor motor;

    public Door(Integer position, Motor motor) {
        this.position = position;
        this.motor = motor;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    public void openFully() {
        while (position < Controller.Controller.getDoorWidth()) {
            motor.setDirection(DirectionEnum.OPEN);
            move();
        }
        motor.setDirection(DirectionEnum.NONE);

    }

    public void close() {
        motor.setDirection(DirectionEnum.CLOSE);
        move();
        motor.setDirection(DirectionEnum.CLOSE);
    }

    public void move() {
        switch (motor.getDirection()) {
            case OPEN:
                position = position + Controller.Controller.doorSpeed;
                break;
            case CLOSE:
                position = position - Controller.Controller.doorSpeed;
                break;
            case NONE:
                break;

        }
    }
}