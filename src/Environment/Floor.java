package Environment;

public class Floor {

    private Integer floor;
    private Boolean liftPresent;
    private Integer position;
    private Boolean button;
    private Door door;

    public Floor(Integer floor, Boolean liftPresent, Integer position, Boolean button, Door door) {
        this.floor = floor;
        this.liftPresent = liftPresent;
        this.position = position;
        this.button = button;
        this.door = door;
    }
    
    

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public Boolean getLiftPresent() {
        return liftPresent;
    }

    public void setLiftPresent(Boolean liftPresent) {
        this.liftPresent = liftPresent;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public Boolean getButton() {
        return button;
    }

    public void setButton(Boolean button) {
        this.button = button;
    }

    public Door getDoor() {
        return door;
    }

    public void setDoor(Door door) {
        this.door = door;
    }
}