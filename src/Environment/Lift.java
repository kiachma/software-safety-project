package Environment;

import Controller.Controller;
import static Environment.DirectionEnum.NONE;
import java.util.ArrayList;
import java.util.List;

public class Lift {

    private Boolean[] doorButton;
    private Boolean emergencyButton;
    private Integer position;
    private List<Floor> queue;
    private LiftDoor door;
    private Motor motor;
    private Boolean Idle;

    public Lift(Boolean[] doorButton, Boolean emergencyButton, Integer position, List<Floor> queue, LiftDoor door, Motor motor) {
        this.doorButton = doorButton;
        for (int i = 0; i < doorButton.length; i++) {
            this.doorButton[i] = Boolean.FALSE;
        }
        this.emergencyButton = emergencyButton;
        this.position = position;
        this.queue = queue;
        this.door = door;
        this.motor = motor;
        this.Idle = true;
    }

    public Boolean[] getDoorButton() {
        return doorButton;
    }

    public void setDoorButton(Boolean[] doorButton) {
        this.doorButton = doorButton;
    }

    public Boolean getEmergencyButton() {
        return emergencyButton;
    }

    public void setEmergencyButton(Boolean emergencyButton) {
        this.emergencyButton = emergencyButton;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public List<Floor> getQueue() {
        return queue;
    }

    public void setQueue(List<Floor> queue) {
        this.queue = queue;
    }

    public LiftDoor getDoor() {
        return door;
    }

    public void setDoor(LiftDoor door) {
        this.door = door;
    }

    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    public void setIdle(Boolean vacant) {
        this.Idle = vacant;
    }

    public Boolean isIdle() {
        return Idle;
    }

    public void addFloorToQueue(Floor floor) {
        if (queue == null) {
            queue = new ArrayList<>();
        }
        queue.add(floor);
    }

    public void moveToFloor(Floor floor) {
        boolean cont = true;
        while (cont) {
            if (floor.getPosition() < position) {
                motor.setDirection(DirectionEnum.DOWN);
            } else if (floor.getPosition() > position){
                 motor.setDirection(DirectionEnum.UP);
            }
            else{
                motor.setDirection(DirectionEnum.NONE);
                cont=false;
            }
            System.out.println("Moving "+ getMotor().getDirection()+" to floor " + floor.getFloor() +"("+getPosition()+"/"+floor.getPosition()+")");
            move();
            Controller.waitSeconds(1);
        }
    }

    private void move() {
        switch (motor.getDirection()) {
            case DOWN:
                position = position - Controller.liftSpeed;
                break;
            case UP:
                position = position + Controller.liftSpeed;
                break;
            case NONE:
                break;

        }
    }
}