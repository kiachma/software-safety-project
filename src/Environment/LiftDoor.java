package Environment;

public class LiftDoor extends Door {

    private Boolean obstacle;
    private Boolean closed;

    public LiftDoor(Boolean obstacle, Boolean closed) {
        super(0, new Motor(Controller.Controller.doorSpeed, DirectionEnum.NONE));
        this.obstacle = obstacle;
        this.closed = closed;
    }
    
   

    public Boolean getObstacle() {
        return obstacle;
    }

    public void setObstacle(Boolean obstacle) {
        this.obstacle = obstacle;
    }

    public Boolean getClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }
    
    @Override
    public void close() {
        super.close();
        closed=true;
       
    }
    
    @Override
    public void openFully() {
        super.openFully();
        closed=false;
       
    }
}