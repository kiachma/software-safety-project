package Environment;


import Environment.DirectionEnum;

public class Motor {
      
    
    

    private Integer speed;
    private DirectionEnum direction;

    public Motor(Integer speed, DirectionEnum direction) {
        this.speed = speed;
        this.direction = direction;
    }

    
    
    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public DirectionEnum getDirection() {
        return direction;
    }

    public void setDirection(DirectionEnum direction) {
        this.direction = direction;
    }
    
    
}